<?php

define('PAGE_COUNT', 4);

function sec_upper($pageIndex){
    $htm = <<<EOD
<nav class="navbar navbar-expand-lg navbar-light bg-white">
        <a class="navbar-brand" href="#"><img src="img/logo%%201x1.png" width="60px" height="60px"></a>
        <div class="navbar-toggler" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </div>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item %s">
                    <a class="nav-link" href="index.php">About</a>
                </li>
                <li class="nav-item %s">
                    <a class="nav-link" href="products.php">Our Products</a>
                </li>
                <li class="nav-item %s">
                    <a class="nav-link" href="packages.php">Pricing</a>
                </li>
                <li class="nav-item %s">
                    <a class="nav-link" href="packages.php">Customers</a>
                </li>
            </ul>
        </div>
    </nav>
EOD;

    $d = array();
    for($i=1; $i<=PAGE_COUNT; $i++)
        array_push($d, $i==$pageIndex?"active":"");

    return vsprintf($htm, $d);

}

function sec_lower(){
    $htm = <<<EOD
<footer class="page-footer font-small cyan darken-3 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12 py-5 align-items-center justify-content-center d-flex">
                    <div class="flex-center">
                        <a class="fb-ic">
                            <i class="fab fa-facebook-f fa-lg text-white mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="tw-ic">
                            <i class="fab fa-twitter fa-lg text-white mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="gplus-ic">
                            <i class="fab fa-google-plus-g fa-lg text-white mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="li-ic">
                            <i class="fab fa-linkedin-in fa-lg text-white mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="ins-ic">
                            <i class="fab fa-instagram fa-lg text-white mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="pin-ic">
                            <i class="fab fa-pinterest fa-lg text-white fa-2x"> </i>
                        </a>
                    </div>                  
                </div>
                <div class="col-md-12 py-1 align-items-center justify-content-center d-flex">
                    <div class="flex-center d-block">                                             
                        <h1 class="display-5 text-light"><span class="text-">CALL | </span> +94 71 5976 199</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright text-center text-light  py-3">© 2018 Copyright Cyberware.LK
        </div>
    </footer>
EOD;
    return $htm;
}