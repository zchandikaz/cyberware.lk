<?php include '_include.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>CYBERWARE.LK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="plugin/web-fonts-with-css/css/fontawesome-all.min.css"/>
</head>
<body>
<?= sec_upper(3) ?>
<div class="container-fluid">
    <h1 class="display-2 text-uppercase text-center mt-4">Our Packages</h1>
    <div class="row mt-5 mb-5">
        <div class="col-3">
            <div class="card bg-light">
                <div class="card-header"><h3 class="display-5">BASIC</h3></div>
                <div class="card-body">
                    <p class="card-text">Basic web site for personal or small business plan.</p>
                </div>
                <div class="card-footer">
                    <small>Rs. 15 000.00 - 35 000.00</small>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card bg-info text-light">
                <div class="card-header"><h3 class="display-5">STANDARD</h3></div>
                <div class="card-body">
                    <p class="card-text">Better web site with more facilities which will help to achieve your goal.</p>
                </div>
                <div class="card-footer">
                    <small>Rs. 35 000.00 - 65 000.00</small>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card bg-primary text-light">
                <div class="card-header"><h3 class="display-5">PLUS</h3></div>
                <div class="card-body">
                    <p class="card-text">Web site with full facilities with creative design.</p>
                </div>
                <div class="card-footer">
                    <small>Rs. 65 000.00 - 100 000.00</small>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card bg-dark text-light">
                <div class="card-header"><h3 class="display-5">PREMIUM</h3></div>
                <div class="card-body">
                    <p class="card-text">Web site with all features and unique features for your own.</p>
                </div>
                <div class="card-footer">
                    <small>Rs. 100 000.00 - XXX</small>
                </div>
            </div>
        </div>
    </div>
</div>

<?= sec_lower() ?>

</body>
<script src="js/jquery-3.3.1.min.js"  type="text/javascript" language="JavaScript"></script>
<script src="js/bootstrap.min.js"  type="text/javascript" language="JavaScript"></script>
<script src="js/script.js"></script>
</html>
