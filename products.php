<?php include '_include.php' ?>
<!DOCTYPE html>
<html>
<head>
  <title>CYBERWARE.LK</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="plugin/web-fonts-with-css/css/fontawesome-all.min.css"/>
</head>
<body>
    <?= sec_upper(2) ?>
    <div class="container-fluid">
        <div class="row mb-md-5">
            <div class="col-md-6">
                <div class="jumbotron jumbotron-fluid bg-white">
                    <div class="container">
                        <h1 class="display-4">Creative Web Sites</h1>
                        <p class="lead">You'll have creative smart web site which let your go forward among your competitors.You'll grant responsible web site with proper animations.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="align-items-center justify-content-center d-flex h-100">
                    <img src="img/webdesign1.png" class="img-fluid"/>
                </div>
            </div>
        </div>
        <div class="row mb-md-5">
            <div class="col-md-6 d-none d-sm-block">
                <div class="align-items-center justify-content-center d-flex h-100">
                    <img src="img/desktopapp1.png" class="img-fluid"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="jumbotron jumbotron-fluid bg-white">
                    <div class="container">
                        <h1 class="display-4">Desktop Applications</h1>
                        <p class="lead">Powerful desktop applications which will make your works lot easier.You can acquire ERP System, Pos System or Customizable Application according to your requirements.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-sm-none d-block">
                <div class="align-items-center justify-content-center d-flex h-100">
                    <img src="img/desktopapp1.png" class="img-fluid"/>
                </div>
            </div>
        </div>
        <div class="row mb-md-5">
            <div class="col-md-6">
                <div class="jumbotron jumbotron-fluid bg-white">
                    <div class="container">
                        <h1 class="display-4">Mobile Applications</h1>
                        <p class="lead">Let's handle your business while you enjoying your life. Mobile applications which suit to any mobile device.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="align-items-center justify-content-center d-flex h-100">
                    <img src="img/mobileapp1.png" class="img-fluid"/>
                </div>
            </div>
        </div>
    </div>

    <?= sec_lower() ?>

  </body>
  <script src="js/jquery-3.3.1.min.js"  type="text/javascript" language="JavaScript"></script>
  <script src="js/bootstrap.min.js"  type="text/javascript" language="JavaScript"></script>
  <script src="js/script.js"></script>
  </html>
