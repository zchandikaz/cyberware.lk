<?php include '_include.php' ?>
<!DOCTYPE html>
<html>
<head>
  <title>CYBERWARE.LK</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="plugin/web-fonts-with-css/css/fontawesome-all.min.css"/>
</head>
<body>
    <?= sec_upper(1) ?>
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center d-flex h-100">
            <img src="img/logo%201x1.png" class="img-fluid" width="30%" />
        </div>
        <div class="container">
            <hr/>
            <div class="row mt-5 mb-5 align-items-center justify-content-center d-flex h-100">
                <h1 class="display-4 text-center">Our mission is to open pathway for the cyber world by offering excellence reliable software solutions</h1>
            </div>
        </div>
    </div>

    <?= sec_lower() ?>

  </body>
  <script src="js/jquery-3.3.1.min.js"  type="text/javascript" language="JavaScript"></script>
  <script src="js/bootstrap.min.js"  type="text/javascript" language="JavaScript"></script>
  <script src="js/script.js"></script>
  </html>
